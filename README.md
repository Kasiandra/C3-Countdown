**C3-Countdown**

Dies ist ein einfacher Countdown bis zum nächsten CCC Congress, geschrieben in HTML, CSS und JS.
Bilder entsammen meiner eigener Sammlung, welche ich zum #35C3 schoss.


![https://chaos.social/system/media_attachments/files/002/477/526/original/226b7563d79c2dec.jpeg?1547995907](https://chaos.social/system/media_attachments/files/002/477/526/original/226b7563d79c2dec.jpeg?1547995907)

*Automatisch generierter Screenshot mit wkhtmltox von https://c3.elektrollart.org*